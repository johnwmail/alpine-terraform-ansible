FROM alpine:latest

USER 0

RUN set -ex;
RUN apk update
RUN apk add openssh-client sshpass \
    python3 ansible-core py3-netaddr py3-psutil \
    wget unzip
RUN ansible-galaxy collection install ansible.posix     -p /usr/share/ansible/collections
RUN ansible-galaxy collection install ansible.utils     -p /usr/share/ansible/collections
RUN ansible-galaxy collection install community.crypto  -p /usr/share/ansible/collections
RUN ansible-galaxy collection install community.general -p /usr/share/ansible/collections
RUN rm -f /var/cache/apk/APKINDEX.*

ENV TF_VER="1.7.5"

RUN \
if [ $(apk --print-arch) == 'aarch64' ]; then \
    wget -O /tmp/tf.zip "https://releases.hashicorp.com/terraform/${TF_VER}/terraform_${TF_VER}_linux_arm64.zip"; \
elif [ $(apk --print-arch) == 'x86_64' ]; then \
    wget -O /tmp/tf.zip "https://releases.hashicorp.com/terraform/${TF_VER}/terraform_${TF_VER}_linux_amd64.zip"; \
else \
    echo "Failure to detect arch"; \
    exit 1; \
fi

RUN unzip /tmp/tf.zip && rm -f /tmp/tf.zip
RUN chmod 755 terraform
RUN mv terraform /usr/bin/terraform

RUN adduser -u 1000 -D runner
USER 1000

CMD ["/bin/sh"]

